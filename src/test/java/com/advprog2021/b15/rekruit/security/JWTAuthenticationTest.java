package com.advprog2021.b15.rekruit.security;

import com.advprog2021.b15.rekruit.controller.AuthController;
import com.advprog2021.b15.rekruit.model.UserModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.repository.UserModelRepository;
import com.advprog2021.b15.rekruit.services.AuthServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = {AuthController.class, JWTAuthenticationFilter.class})
class JWTAuthenticationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserModelRepository userModelRepository;

    @MockBean
    private AuthServiceImpl authService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private AuthenticationManager authenticationManager;

    @Test
//    @WithUserDetails(value = "eren.yeager@survey.corps", userDetailsServiceBeanName = "loadUserByUsername")
    void testAuthWithJWTAndReturnUnauthorized() throws Exception {
        Map<String,String> credentials = new HashMap<>();
        credentials.put("password", "dummy_password");
        credentials.put("email", "eren.yeager@survey.corps");
        String jsonReq = new ObjectMapper().writeValueAsString(credentials);

        UserModel user = new UserModel();
        user.setEmail("eren.yeager@survey.corps");
        user.setPassword("dummy_password");
        user.setUserRole(UserModelRole.REKRUTER);
        user.setId(1L);

        when(userModelRepository.findByEmail("eren.yeager@survey.corps")).thenReturn(Optional.of(user));
        when(userDetailsService.loadUserByUsername("eren.yeager@survey.corps")).thenReturn(user);

        mockMvc.perform(
                post("/auth")
                        .content(jsonReq)
        ).andExpect(status().isUnauthorized());
    }
}
