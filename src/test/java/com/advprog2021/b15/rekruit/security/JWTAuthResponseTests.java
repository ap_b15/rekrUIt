package com.advprog2021.b15.rekruit.security;

import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.security.utils.JWTAuthResponse;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class JWTAuthResponseTests {
    private JWTAuthResponse jwtAuthResponse = new JWTAuthResponse(
            "hoover.bertholt@survey.corps",
            "randomTokenString",
            UserModelRole.REKRUTER,
            1L
    );

    @Test
    void getEmailShouldReturnStringEmail() {
        assertEquals("hoover.bertholt@survey.corps", jwtAuthResponse.getEmail());
    }

    @Test
    void getTokenShoulReturnRandomAssignedString() {
        assertEquals("randomTokenString", jwtAuthResponse.getToken());
    }

    @Test
    void getRoleUserRoleShouldReturnRekruterEnum() {
        assertEquals(UserModelRole.REKRUTER, jwtAuthResponse.getRole());
    }

    @Test
    void toStringShouldReturnJSONlikeFormat() {
        assertEquals(
                String.format(
                "{ \"email\": \"%s\", \"token\": \"%s\", \"role\": \"%s\", \"id\": \"%d\"}",
                jwtAuthResponse.getEmail(),
                jwtAuthResponse.getToken(),
                jwtAuthResponse.getRole(),
                jwtAuthResponse.getId()
                ),
                jwtAuthResponse.toString()
        );
    }
}
