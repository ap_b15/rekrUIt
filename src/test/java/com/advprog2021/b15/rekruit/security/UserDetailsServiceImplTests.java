package com.advprog2021.b15.rekruit.security;

import com.advprog2021.b15.rekruit.model.UserModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.repository.UserModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTests {
    @Mock
    private UserModelRepository userModelRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    private UserModel userCreated;

    @BeforeEach
    void setUp() {
        userCreated = new UserModel();
        userCreated.setEmail("ackerman.mikasa@survey.corps");
        userCreated.setPassword("Eren,onegai...");
        userCreated.setUserRole(UserModelRole.PENDAFTAR);

    }

    @Test
    void loadUserByUsernameShouldThrowsUsernameNotFoundException() {
        assertThrows(UsernameNotFoundException.class, () -> {
            userDetailsService.loadUserByUsername("reiner.braun@survey.corps");
        });
    }

    @Test
    void loadUsernameShouldReturnUserDetails() {
        when(userModelRepository.findByEmail("ackerman.mikasa@survey.corps")).thenReturn(Optional.ofNullable((userCreated)));

        UserDetails result = userDetailsService.loadUserByUsername("ackerman.mikasa@survey.corps");
        assertEquals("ackerman.mikasa@survey.corps", result.getUsername());
    }
}
