package com.advprog2021.b15.rekruit.security;

import com.advprog2021.b15.rekruit.security.utils.EmailPasswordAuthRequest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EmailPasswordAuthRequestTests {
    private EmailPasswordAuthRequest emailPasswordAuthRequest = new EmailPasswordAuthRequest(
            "hoover.bertholt@survey.corps",
            "annieeee"
    );

    private EmailPasswordAuthRequest emptyAuthRequest = new EmailPasswordAuthRequest();

    @Test
    void getEmailShouldReturnEmail() {
        assertEquals("hoover.bertholt@survey.corps", emailPasswordAuthRequest.getEmail());
    }

    @Test
    void getPasswordShouldReturnPassword() {
        assertEquals("annieeee", emailPasswordAuthRequest.getPassword());
    }

    @Test
    void setEmailPasswordEmptyAuthRequestThenGetEmailPassword() {
        emptyAuthRequest.setEmail("hoover.bertholt@survey.corps");
        emptyAuthRequest.setPassword("annieeee");
        assertEquals("hoover.bertholt@survey.corps", emptyAuthRequest.getEmail());
        assertEquals("annieeee", emptyAuthRequest.getPassword());
    }
}
