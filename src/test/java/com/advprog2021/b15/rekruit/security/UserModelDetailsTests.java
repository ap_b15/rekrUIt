package com.advprog2021.b15.rekruit.security;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.UserModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserModelDetailsTests {

    private final UserModel userDetails = new PendaftarModel();

    @BeforeEach
    public void setUp() {
        userDetails.setId(1L);
        userDetails.setEmail("ackerman.levi@survey.corps");
        userDetails.setPassword("admin123");
        userDetails.setUserRole(UserModelRole.PENDAFTAR);
    }

    @Test
    void idShouldReturnLongIdEntity() {
        assertEquals(1L, userDetails.getId());
    }

    @Test
    void emailShouldReturnString() {
        assertNotNull(userDetails.getEmail());
    }

    @Test
    void emailShouldReturnUserDetailsEmaik() {
        assertEquals("ackerman.levi@survey.corps", userDetails.getEmail());
    }

    @Test
    void getPasswordShouldReturnAString() {
        assertNotNull(userDetails.getPassword());
    }

    @Test
    void getPasswordShouldReturnUserDetailsPassword(){
        assertEquals("admin123", userDetails.getPassword());
    }

    @Test
    void userRoleShouldHaveEnum() {
        assertNotNull(userDetails.getUserRole());
    }

    @Test
    void userRoleShouldHasPendaftarEnum() {
        assertEquals(UserModelRole.PENDAFTAR, userDetails.getUserRole());
    }

    @Test
    void getUsernameShouldNotReturnNull() {
        assertNotNull(userDetails.getUsername());
    }

    @Test
    void getUsernameShouldReturnUserDetailsUsername(){
        assertEquals("ackerman.levi@survey.corps", userDetails.getUsername());
    }

    @Test
    void isAccountNotExpiredSHouldReturnTrue(){
        assertTrue(userDetails.isAccountNonExpired());
    }

    @Test
    void isAccountNotLockedShouldReturnTrue() {
        assertTrue(userDetails.isAccountNonLocked());
    }

    @Test
    void isCredentialsNotExpiredShouldReturnTrue() {
        assertTrue(userDetails.isCredentialsNonExpired());
    }

    @Test
    void isEnableShouldReturnTrue() {
        assertTrue(userDetails.isEnabled());
    }

    @Test
    void isReturningAuthority() {
        assertEquals(userDetails.getAuthorities(), userDetails.getAuthorities());
    }

}
