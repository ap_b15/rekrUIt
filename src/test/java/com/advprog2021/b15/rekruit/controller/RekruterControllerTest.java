package com.advprog2021.b15.rekruit.controller;

import com.advprog2021.b15.rekruit.model.*;
import com.advprog2021.b15.rekruit.security.UserDetailsServiceImpl;
import com.advprog2021.b15.rekruit.services.MendaftarService;
import com.advprog2021.b15.rekruit.services.PengumumanService;
import com.advprog2021.b15.rekruit.services.RekruiterService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import javax.print.attribute.standard.Media;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

import static com.advprog2021.b15.rekruit.security.utils.SecurityConstants.EXPIRATION_TIME;
import static com.advprog2021.b15.rekruit.security.utils.SecurityConstants.SECRET;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = RekruterController.class)
class RekruterControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RekruiterService rekruiterService;

    @MockBean
    private MendaftarService mendaftarService;

    @MockBean
    private PengumumanService pengumumanService;

    @MockBean
    private  PasswordEncoder passwordEncoder;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    private RekruitmenModel rekruitmenModel;
    private PendaftarModel pendaftar;
    private RekruterModel rekruterModel;
    private RekruitmenModel tempChange;
    private PengumumanModel pengumumanModel;
    private MendaftarModel mendaftarModel;
    private Set<RekruitmenModel> rekruitmenModelSet = new HashSet<>();

    private static String jsonWebToken;

    @BeforeEach
    public void setUp() {
        rekruterModel = new RekruterModel();
        rekruterModel.setId(1L);
        rekruterModel.setEmail("semogaadproA@gmail.com");
        rekruterModel.setPassword("aminpalingserius");
        rekruterModel.setUserRole(UserModelRole.REKRUTER);
        rekruterModel.setNamaRekruter("Pacilian");
        rekruterModel.setDeskripsiRekruter("test doang kok");
        rekruterModel.setRekruitmenModelSet(Collections.emptySet());

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(rekruterModel);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");

        rekruitmenModelSet.add(rekruitmenModel);
        rekruterModel.setRekruitmenModelSet(rekruitmenModelSet);


        RekruitmenModel tempChange = new RekruitmenModel();
        tempChange.setId(1L);
        tempChange.setJudul("Pencari Uang Pendaftar");
        tempChange.setDidaftarOleh(null);
        tempChange.setRekruiter(rekruterModel);
        tempChange.setStatus("Sudah Dibuka");
        tempChange.setNarahubung("081219447378");
        tempChange.setDeskripsiTugas("review risol yang enak");
        tempChange.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        tempChange.setSyaratKetentuan("mahasiswa pacil");
        tempChange.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        tempChange.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setLinkWawancara("www.google.com");

        mendaftarModel = new MendaftarModel();
        mendaftarModel.setId("19000000");
        mendaftarModel.setLinkCV("linkcv.com");
        mendaftarModel.setLinkTugas("linktugas.com");
        mendaftarModel.setStatusPenerimaan("BelumDiterima");
        mendaftarModel.setNilai(80);
        mendaftarModel.setPendaftarModel(null);
        mendaftarModel.setRekruitmenModel(null);

        pendaftar = new PendaftarModel();
        pendaftar.setId(2L);
        pendaftar.setEmail("dummy@gmail.com");
        pendaftar.setPassword("dummypassword");
        pendaftar.setUserRole(UserModelRole.PENDAFTAR);
        pendaftar.setNamaLengkap("dummy bin titan");
        pendaftar.setNpm("1906272727");
        pendaftar.setFakultas("FASILKOM");
        pendaftar.setKontak("dummyGanteng");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private void helperPostLoginAuthWithJWT() {
        jsonWebToken =  JWT.create()
                .withSubject(rekruterModel.getEmail())
                .withClaim("role",rekruterModel.getUserRole().name())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
    }

    @Test
    void testControllerGetAllRekruitmen() throws Exception {
        when(rekruiterService.getAllRekruitmenModels()).thenReturn(new ArrayList<>(rekruitmenModelSet));
        mvc.perform(
                get("/rekruter/rekruitmen").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0]['id']]").value("1"));
    }

    @Test
    void testControllerGetRekruitmenAndExpectForbidden() throws Exception {
        when(rekruiterService.getRekruitmenModels(1)).thenReturn(new ArrayList<>(rekruitmenModelSet));
        mvc.perform(
                get("/rekruter/1").contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isForbidden());
    }

    @Test
    void testControllerGetRekruitmen() throws Exception {
        when(rekruiterService.getRekruitmenModels(1)).thenReturn(new ArrayList<>(rekruitmenModelSet));
        helperPostLoginAuthWithJWT(); // Run once, execute anywhere after this test
        mvc.perform(
                get("/rekruter/1").contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer " + jsonWebToken)
        )
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0]['id']]").value("1"));

    }

    @Test
    void testControllerCreateRekruitmen() throws Exception {
        when(rekruiterService.createRekruitmen(1, rekruitmenModel)).thenReturn(rekruitmenModel);
        RekruterModel rekruiter = new RekruterModel();
        rekruiter.setId(1L);
        rekruiter.setEmail("erwin.smith@survey.corps");
        rekruiter.setPassword("aminpalingserius");
        rekruiter.setNamaRekruter("Pacilian");
        rekruiter.setDeskripsiRekruter("test doang kok");
        rekruiter.setRekruitmenModelSet(Collections.emptySet());

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1);
        rekruitmenModel.setRekruiter(rekruiter);
        rekruitmenModel.setJudul("Open Tender Pemira UI 2021");
        rekruitmenModel.setStatus("Belum Dibuka");
        rekruitmenModel.setNarahubung("+6285712349876 (DPM UI)");
        rekruitmenModel.setDeskripsiTugas("Katanya menyelenggarakan demokrasi di UI");
        rekruitmenModel.setDeskripsiPekerjaan("Menjadi pion politik golongan");
        rekruitmenModel.setSyaratKetentuan("Minimal tingkat 2");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("http://permira.ui.ac.id");

        mvc.perform(post("/rekruter/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(rekruitmenModel)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerUpdateRekruitmen() throws Exception {
        when(rekruiterService.updateRekruitmen(1,tempChange )).thenReturn(tempChange);
        RekruterModel rekruiter = new RekruterModel();
        rekruiter.setId(1L);
        rekruiter.setEmail("erwin.smith@survey.corps");
        rekruiter.setPassword("aminpalingserius");
        rekruiter.setNamaRekruter("Pacilian");
        rekruiter.setDeskripsiRekruter("test doang kok");
        rekruiter.setRekruitmenModelSet(Collections.emptySet());

        tempChange = new RekruitmenModel();
        tempChange.setId(1);
        tempChange.setRekruiter(rekruiter);
        tempChange.setJudul("Open Tender Pemira UI 2021");
        tempChange.setStatus("Sudah Dibuka");
        tempChange.setNarahubung("+6285712349876 (DPM UI)");
        tempChange.setDeskripsiTugas("Katanya menyelenggarakan demokrasi di UI");
        tempChange.setDeskripsiPekerjaan("Menjadi pion politik golongan");
        tempChange.setSyaratKetentuan("Minimal tingkat 2");
        tempChange.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        tempChange.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setLinkWawancara("http://permira.ui.ac.id");

        mvc.perform(put("/rekruter/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(tempChange)))
                .andExpect(status().isOk());
    }
    @Test
    void testControllerCreatePengumuman() throws Exception {
        when(rekruiterService.createPengumuman(pengumumanModel, 1)).thenReturn(pengumumanModel);
        RekruterModel rekruiter = new RekruterModel();
        rekruiter.setId(1L);
        rekruiter.setEmail("erwin.smith@survey.corps");
        rekruiter.setPassword("aminpalingserius");
        rekruiter.setNamaRekruter("Pacilian");
        rekruiter.setDeskripsiRekruter("test doang kok");
        rekruiter.setRekruitmenModelSet(Collections.emptySet());

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1);
        rekruitmenModel.setRekruiter(rekruiter);
        rekruitmenModel.setJudul("Open Tender Pemira UI 2021");
        rekruitmenModel.setStatus("Belum Dibuka");
        rekruitmenModel.setNarahubung("+6285712349876 (DPM UI)");
        rekruitmenModel.setDeskripsiTugas("Katanya menyelenggarakan demokrasi di UI");
        rekruitmenModel.setDeskripsiPekerjaan("Menjadi pion politik golongan");
        rekruitmenModel.setSyaratKetentuan("Minimal tingkat 2");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("http://permira.ui.ac.id");

        pengumumanModel = new PengumumanModel();
        pengumumanModel.setId(1);
        pengumumanModel.setWaktu(1);
        pengumumanModel.setIsi("Kabar baik tugas nya di extend yeay");
        pengumumanModel.setJudul("Extend Tugas");
        pengumumanModel.setRekruitmenModel(rekruitmenModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(post("/rekruter/pengumuman/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(pengumumanModel)))
                .andExpect(status().isOk());
        System.out.print(mapToJson(pengumumanModel));
    }
    @Test
    void testMenilaiRekruitmen() throws Exception {
        when(rekruiterService.menilaiRekruitmen("1", "BelumDiterima", 80)).thenReturn(mendaftarModel);
        mvc.perform(post("/rekruter/menilai/1/BelumDiterima/80")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(mendaftarModel)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerGetRekruiterModel() throws Exception {
        when(rekruiterService.getRekruiterModel(1)).thenReturn(rekruterModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/rekruter/get/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(rekruterModel)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerGetRekruitmenByID() throws Exception {
        when(rekruiterService.getRekruitmenModelByID(1)).thenReturn(rekruitmenModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/rekruter/rekruitmen/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(rekruitmenModel)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerDeleteRekruitmen() throws Exception {
        when(rekruiterService.deleteRekruitmen(1, 1)).thenReturn("Berhasil");
        helperPostLoginAuthWithJWT();
        mvc.perform(delete("/rekruter/rekruitmen/1/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson("Berhasil")))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerGetMendaftar() throws Exception{
        when(mendaftarService.getMendaftarById("1906272727-1")).thenReturn(mendaftarModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/rekruter/mendaftar/1906272727/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerGetMendaftarByPart() throws Exception{
        ArrayList<MendaftarModel> dummy = new ArrayList<MendaftarModel>();
        dummy.add(mendaftarModel);
        when(mendaftarService.getMendaftarByPartId("-1")).thenReturn(dummy);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/rekruter/mendaftar/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken))
                .andExpect(status().isOk());
    }
}
