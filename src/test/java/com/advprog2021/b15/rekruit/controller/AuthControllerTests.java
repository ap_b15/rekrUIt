package com.advprog2021.b15.rekruit.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.advprog2021.b15.rekruit.security.UserDetailsServiceImpl;
import com.advprog2021.b15.rekruit.services.AuthServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.HashMap;
import java.util.Map;

@WebMvcTest(controllers = AuthController.class)
class AuthControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthServiceImpl authService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
	private ObjectMapper mapper;

    @Test
    void testRegisterPendaftar() throws Exception {

        Map<String,String> credentials = new HashMap<>();
        credentials.put("email", "eren.yeager@survey.corps");
        credentials.put("password", "dummy_password");
        credentials.put("userRole", "PENDAFTAR");
        credentials.put("npm", "200612349876");
        credentials.put("namaLengkap", "Eren Yeager");
        credentials.put("fakultas", "Ilmu Komputer");
        credentials.put("kontak", "+6288812349876");

        String jsonReq = mapper.writeValueAsString(credentials);

        MvcResult response = mockMvc.perform(
            post("/auth/register/pendaftar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReq)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("registerPendaftar")).andReturn();

    }

    @Test
    void testRegisterRekruter() throws Exception {

        Map<String,String> credentials = new HashMap<>();
        credentials.put("email", "erwin.smith@survey.corps");
        credentials.put("password", "dummy_password");
        credentials.put("userRole", "REKRUTER");
        credentials.put("namaRekruter", "Erwin Smith");
        credentials.put("deskripsiRekruter", "TATAKAEEE, Shinzou Sasageyoo!");

        String jsonReq = mapper.writeValueAsString(credentials);

        MvcResult response = mockMvc.perform(
            post("/auth/register/rekruter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReq)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("registerRekruter")).andReturn();

    }

}
