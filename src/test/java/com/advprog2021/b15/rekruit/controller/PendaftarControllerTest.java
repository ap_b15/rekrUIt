package com.advprog2021.b15.rekruit.controller;


import com.advprog2021.b15.rekruit.model.MendaftarModel;
import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.security.UserDetailsServiceImpl;
import com.advprog2021.b15.rekruit.services.MendaftarServiceImpl;
import com.advprog2021.b15.rekruit.services.PendaftarServiceImpl;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;

import java.util.Date;

import static com.advprog2021.b15.rekruit.security.utils.SecurityConstants.EXPIRATION_TIME;
import static com.advprog2021.b15.rekruit.security.utils.SecurityConstants.SECRET;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PendaftarController.class)
class PendaftarControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PendaftarServiceImpl pendaftarService;

    @MockBean
    private MendaftarServiceImpl mendaftarService;

    @MockBean
    private  UserDetailsServiceImpl userDetailsService;

    @MockBean
    private  PasswordEncoder passwordEncoder;

    private PendaftarModel pendaftar;
    private RekruitmenModel rekruitmenModel;
    private MendaftarModel mendaftarModel;

    private String jsonWebToken;

    @BeforeEach
    public  void setUp() {
        pendaftar = new PendaftarModel();
        pendaftar.setId(2L);
        pendaftar.setEmail("dummy@gmail.com");
        pendaftar.setPassword("dummypassword");
        pendaftar.setUserRole(UserModelRole.PENDAFTAR);
        pendaftar.setNamaLengkap("dummy bin titan");
        pendaftar.setNpm("1906272727");
        pendaftar.setFakultas("FASILKOM");
        pendaftar.setKontak("dummyGanteng");

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(null);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");
        rekruitmenModel.setPengumumanModelSet(null);

        mendaftarModel = new MendaftarModel();
        mendaftarModel.setId("21");
        mendaftarModel.setPendaftarModel(pendaftar);
        mendaftarModel.setRekruitmenModel(rekruitmenModel);
        mendaftarModel.setStatusPenerimaan(null);
        mendaftarModel.setNilai(0);
        mendaftarModel.setLinkCV(null);
        mendaftarModel.setLinkTugas(null);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private void helperPostLoginAuthWithJWT() {
        jsonWebToken =  JWT.create()
                .withSubject(pendaftar.getEmail())
                .withClaim("role",pendaftar.getUserRole().name())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
    }

    @Test
    void testControllerGetPendaftarById() throws  Exception {
        when(pendaftarService.getPendaftarById(2L)).thenReturn(pendaftar);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/pendaftar/2")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.npm").value("1906272727"));
    }

    @Test
    void testControllerUpdatePendaftar() throws Exception {
        pendaftar.setFakultas("FK");
        when(pendaftarService.updateDataPendaftar(pendaftar.getId(), pendaftar)).thenReturn(pendaftar);
        helperPostLoginAuthWithJWT();
        mvc.perform(put("/pendaftar/2")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(pendaftar)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerSortRekruitmen() throws  Exception {
        long kode = 1;
        when(pendaftarService.sortRekruitmen(pendaftar.getId(),kode)).thenReturn(pendaftar);
        helperPostLoginAuthWithJWT();
        mvc.perform(post("/pendaftar/sort/2/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(pendaftar)))
                .andExpect(jsonPath("$.npm").value("1906272727"));
    }

    @Test
    void testControllerPostMendaftar() throws Exception{
        when(mendaftarService.postMendaftar(pendaftar.getId(),1)).thenReturn(mendaftarModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(post("/pendaftar/2/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerUpdateMendaftar() throws Exception{
        when(mendaftarService.updateLinkMendaftar("21", mendaftarModel)).thenReturn(mendaftarModel);
        helperPostLoginAuthWithJWT();
        mvc.perform(put("/pendaftar/mendaftar/21").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken)
                .content(mapToJson(mendaftarModel)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerDeleteMendaftar() throws Exception{
        when(mendaftarService.deleteMendaftar(2,"1")).thenReturn(null);
        helperPostLoginAuthWithJWT();
        mvc.perform(delete("/pendaftar/2/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerGetMendaftar() throws Exception{
        when(mendaftarService.getMendaftarById(pendaftar.getId()+"-1")).thenReturn(null);
        helperPostLoginAuthWithJWT();
        mvc.perform(get("/pendaftar/mendaftar/2/1").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer " + jsonWebToken))
                .andExpect(status().isOk());
    }

}
