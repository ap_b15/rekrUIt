package com.advprog2021.b15.rekruit.controller;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.security.UserDetailsServiceImpl;
import com.advprog2021.b15.rekruit.services.PengumumanService;
import com.advprog2021.b15.rekruit.services.PengumumanServiceImpl;
import com.advprog2021.b15.rekruit.services.RekruiterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import javax.print.attribute.standard.Media;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = PengumumanController.class)
class   PengumumanControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PengumumanServiceImpl pengumumanService;

    @MockBean
    private RekruiterService rekruiterService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    private RekruitmenModel rekruitmenModel;
    private RekruterModel rekruterModel;
    private PengumumanModel pengumumanModel;
    private Set<PengumumanModel> pengumumanModelSet = new HashSet<>();

    private static String jsonWebToken;

    PengumumanControllerTests() {
    }

    @BeforeEach
    public void setUp(){
        rekruterModel = new RekruterModel();
        rekruterModel.setId(1L);
        rekruterModel.setEmail("semogaadproA@gmail.com");
        rekruterModel.setPassword("aminpalingserius");
        rekruterModel.setUserRole(UserModelRole.REKRUTER);
        rekruterModel.setNamaRekruter("Pacilian");
        rekruterModel.setDeskripsiRekruter("test doang kok");
        rekruterModel.setRekruitmenModelSet(Collections.emptySet());

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(rekruterModel);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");
        rekruitmenModel.setPengumumanModelSet( pengumumanModelSet);

        pengumumanModel = new PengumumanModel();
        pengumumanModel.setId(1);
        var waktu =Timestamp.valueOf(LocalDateTime.now()).getTime();
        pengumumanModel.setWaktu(waktu);
        pengumumanModel.setJudul("Update Tugas Video");
        pengumumanModel.setIsi("Tugas Video di extend yeay");
        pengumumanModelSet.add(pengumumanModel);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String mapToJsonForPengumuman(PengumumanModel pengumumanModel)  {
        return "{\n" +
                "    \"waktu\" :\"" +pengumumanModel.getWaktu()+"\",\n" +
                "    \"judul\" :\"" + pengumumanModel.getJudul()+"\",\n" +
                "    \"isi\" :\"" +pengumumanModel.getIsi()+"\"\n" +
                "}";
    }

    @Test
    void testControllerGetPostPengumumanById() throws Exception{
        when(pengumumanService.getPengumumanById(1)).thenReturn(pengumumanModel);
        mockMvc.perform(get("/pengumuman/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));

    }

    @Test
    void testControllerUpdatePengumuman() throws Exception{
        when(pengumumanService.updatePengumuman(pengumumanModel, pengumumanModel.getId())).thenReturn(pengumumanModel);
        pengumumanModel = new PengumumanModel();
        pengumumanModel.setId(1);
        var waktu =Timestamp.valueOf(LocalDateTime.now()).getTime();
        pengumumanModel.setWaktu(waktu);
        pengumumanModel.setJudul("Update Tugas Video");
        pengumumanModel.setIsi("Tugas Video di extend yeay");

        mockMvc.perform(put("/pengumuman/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJsonForPengumuman(pengumumanModel)))
                .andExpect(status().isOk());


    }
    @Test
    void testControllerDeletePengumuman() throws Exception {
        mockMvc.perform(delete("/pengumuman/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
    @Test
    void testControllerGetPostPengumumanByIdRekuitment() throws Exception{
        when(rekruiterService.getListPengumumanModels(1)).thenReturn(new ArrayList<>(pengumumanModelSet));

        mockMvc.perform(get("/pengumuman/rekruitmen/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0]['id']]").value("1"));System.out.print(pengumumanModelSet);

    }


}

