package com.advprog2021.b15.rekruit.services;


import com.advprog2021.b15.rekruit.model.MendaftarModel;
import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.repository.MendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MendaftarServiceImplTest {
    @Mock
    private PendaftarModelRepository pendaftarRepository;

    @Mock
    private RekruitmenModelRepository rekruitmenRepository;

    @Mock
    private MendaftarModelRepository mendaftarModelRepository;

    @InjectMocks
    private PendaftarServiceImpl pendaftarService;

    @InjectMocks
    private MendaftarServiceImpl mendaftarService;

    private PendaftarModel pendaftarModel;
    private RekruitmenModel rekruitmenModel;
    private MendaftarModel mendaftarModel;

    @BeforeEach
    public void setUp() {
        Set<RekruitmenModel> rekruitmenModelSet = new HashSet<>(new ArrayList<>());
        Set<PendaftarModel> pendaftarModelSet = new HashSet<>(new ArrayList<>());
        pendaftarModel = new PendaftarModel();
        pendaftarModel.setId(1);
        pendaftarModel.setEmail("Dummy@gmail.com");
        pendaftarModel.setPassword("dummyPassword");
        pendaftarModel.setNamaLengkap("Dummy Bin Titan");
        pendaftarModel.setNpm("1906282831");
        pendaftarModel.setFakultas("FASILKOM");
        pendaftarModel.setKontak("dummyku21");
        pendaftarModel.setMendaftarRekruitmen(null);

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(2);
        rekruitmenModel.setJudul("Dummy Event");
        rekruitmenModel.setStatus("Belum Dibuka");
        rekruitmenModel.setNarahubung("Dummy People");
        rekruitmenModel.setDeskripsiPekerjaan("Dummy Jobdesc");
        rekruitmenModel.setDeskripsiTugas("Dummy Tasks");
        rekruitmenModel.setSyaratKetentuan("Dummy Terms and Conditions");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-02-26 09:00:00"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-02-26 23:00:00"));
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setLinkWawancara("www.google.com");

        rekruitmenModelSet.add(rekruitmenModel);
        pendaftarModel.setMendaftarRekruitmen(rekruitmenModelSet);

        pendaftarModelSet.add(pendaftarModel);
        rekruitmenModel.setDidaftarOleh(pendaftarModelSet);

        mendaftarModel = new MendaftarModel();
        mendaftarModel.setLinkTugas("www.youtube.com");
        mendaftarModel.setLinkCV("drive.com");
        mendaftarModel.setId("1-2");
        mendaftarModel.setNilai(100);
        mendaftarModel.setPendaftarModel(pendaftarModel);
        mendaftarModel.setRekruitmenModel(rekruitmenModel);
        mendaftarModel.setStatusPenerimaan("Diterima");
    }

    @Test
    void testServicePostMendaftar() {
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.ofNullable(pendaftarModel));
        when(rekruitmenRepository.findById(rekruitmenModel.getId())).thenReturn(rekruitmenModel);

        MendaftarModel resultMendaftar = mendaftarService.postMendaftar(pendaftarModel.getId(),rekruitmenModel.getId());
        String idResult = resultMendaftar.getId();
        assertEquals("1-2", idResult);
    }

    @Test
    void testServiceUpdateNilaiDanStatusTest() {
        MendaftarModel dummy;
        when(mendaftarModelRepository.findById("1-2")).thenReturn(Optional.of(mendaftarModel));
        dummy = mendaftarService.updateNilaiMendaftar("1-2", mendaftarModel);
        assertEquals(dummy,mendaftarModel);
    }

    @Test
    void testServiceUpdateNilaiDanStatusTestReturnNull() {
        assertEquals(null, mendaftarService.updateNilaiMendaftar("1-2", mendaftarModel));
    }



    @Test
    void testServiceUpdateLinkCVTest() {
        MendaftarModel dummy;
        when(mendaftarModelRepository.findById("1-2")).thenReturn(Optional.of(mendaftarModel));
        dummy = mendaftarService.updateLinkMendaftar("1-2", mendaftarModel);
        assertEquals(dummy,mendaftarModel);
    }

    @Test
    void testServiceUpdateLinkCVTestReturnNull() {
        assertEquals(null, mendaftarService.updateLinkMendaftar("1-2", mendaftarModel));
    }

    @Test
    void testServiceDeleteMendaftar() {
        MendaftarModel dummy;
        when(mendaftarModelRepository.findById("1-2")).thenReturn(Optional.of(mendaftarModel));
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.ofNullable(pendaftarModel));
        when(rekruitmenRepository.findById(rekruitmenModel.getId())).thenReturn(rekruitmenModel);

        assertEquals(null, mendaftarService.deleteMendaftar(pendaftarModel.getId(),"2"));
    }

    @Test
    void testServiceGetMendaftarById() {
        when(mendaftarModelRepository.findById("1-2")).thenReturn(Optional.of(mendaftarModel));
        MendaftarModel dummy = mendaftarService.getMendaftarById("1-2");

        assertEquals(mendaftarModel, dummy);
    }

    @Test
    void testServiceGetMendaftarByPartId() {
        List<MendaftarModel> dummy = new ArrayList<MendaftarModel>();
        dummy.add(mendaftarModel);
        when(mendaftarModelRepository.findAll()).thenReturn(dummy);

        assertEquals(1, mendaftarService.getMendaftarByPartId("1-2").size());
    }


}
