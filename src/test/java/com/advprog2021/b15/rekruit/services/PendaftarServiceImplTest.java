package com.advprog2021.b15.rekruit.services;


import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
class PendaftarServiceImplTest {
    @Mock
    private PendaftarModelRepository pendaftarRepository;

    @Mock
    private RekruitmenModelRepository rekruitmenRepository;

    @InjectMocks
    private PendaftarServiceImpl pendaftarService;

    private PendaftarModel pendaftarModel;
    private RekruitmenModel rekruitmenModel;
    private RekruitmenModel rekruitmenModel2;

    @BeforeEach
    public void setUp() {
        Set<RekruitmenModel> rekruitmenModelSet = new HashSet<>(new ArrayList<>());
        Set<PendaftarModel> pendaftarModelSet = new HashSet<>(new ArrayList<>());
        pendaftarModel = new PendaftarModel();
        pendaftarModel.setId(1);
        pendaftarModel.setEmail("Dummy@gmail.com");
        pendaftarModel.setPassword("dummyPassword");
        pendaftarModel.setNamaLengkap("Dummy Bin Titan");
        pendaftarModel.setNpm("1906282831");
        pendaftarModel.setFakultas("FASILKOM");
        pendaftarModel.setKontak("dummyku21");
        pendaftarModel.setMendaftarRekruitmen(null);

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(2);
        rekruitmenModel.setJudul("Event Keren");
        rekruitmenModel.setStatus("Belum Dibuka");
        rekruitmenModel.setNarahubung("Dummy People");
        rekruitmenModel.setDeskripsiPekerjaan("Dummy Jobdesc");
        rekruitmenModel.setDeskripsiTugas("Dummy Tasks");
        rekruitmenModel.setSyaratKetentuan("Dummy Terms and Conditions");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-02-26 09:00:00"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-02-26 22:00:00"));
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setLinkWawancara("www.google.com");

        rekruitmenModel2 = new RekruitmenModel();
        rekruitmenModel2.setId(3);
        rekruitmenModel2.setJudul("Event Dummy");
        rekruitmenModel2.setStatus("Belum Dibuka");
        rekruitmenModel2.setNarahubung("Dummy People");
        rekruitmenModel2.setDeskripsiPekerjaan("Dummy Jobdesc");
        rekruitmenModel2.setDeskripsiTugas("Dummy Tasks");
        rekruitmenModel2.setSyaratKetentuan("Dummy Terms and Conditions");
        rekruitmenModel2.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel2.setDueDateRegistrasi(Timestamp.valueOf("2021-02-26 09:00:00"));
        rekruitmenModel2.setDueDateTugas(Timestamp.valueOf("2021-02-26 23:00:00"));
        rekruitmenModel2.setDidaftarOleh(null);
        rekruitmenModel2.setLinkWawancara("www.google.com");

        rekruitmenModelSet.add(rekruitmenModel);
        rekruitmenModelSet.add(rekruitmenModel2);
        pendaftarModel.setMendaftarRekruitmen(rekruitmenModelSet);

        pendaftarModelSet.add(pendaftarModel);
        rekruitmenModel.setDidaftarOleh(pendaftarModelSet);
    }

    @Test
    void testServiceGetPendaftarByNpmNotNull() {
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.of(pendaftarModel));
        PendaftarModel resultPendaftar = pendaftarService.getPendaftarById(pendaftarModel.getId());
        assertEquals(pendaftarModel.getNpm(), resultPendaftar.getNpm());
    }

    @Test
    void testServiceGetPendaftarByNpmReturnNull() {
        assertEquals(null, pendaftarService.getPendaftarById(pendaftarModel.getId()));
    }

    @Test
    void testServiceUpdatePendaftarReturnNotNull() {
        PendaftarModel dummy;
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.of(pendaftarModel));
        dummy = pendaftarService.updateDataPendaftar(pendaftarModel.getId(), pendaftarModel);
        assertEquals(pendaftarModel, dummy);
    }

    @Test
    void testServiceUpdatePendaftarReturnNull() {
        assertNull(pendaftarService.updateDataPendaftar(pendaftarModel.getId(), pendaftarModel));
    }

    @Test
    void testMendaftarRekruitmen() {
        long id = 1;
        Set<RekruitmenModel> listDummy = new HashSet<>();
        listDummy.add(rekruitmenModel);
        listDummy.add(rekruitmenModel2);
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.of(pendaftarModel));
        when(rekruitmenRepository.findById(id)).thenReturn(rekruitmenModel);
        pendaftarService.mendaftarRekruitmen(id,pendaftarModel.getId());
        assertEquals(listDummy, pendaftarModel.getMendaftarRekruitmen());
    }

    @Test
    void testUpdateForRekruitmen() {
        String dummy = "Ini pengumuman dummy";
        assertEquals(dummy, pendaftarService.update(dummy));
    }

    @Test
    void testSortRekruitmenServiceReturnNotNull() {
        when(pendaftarRepository.findById(pendaftarModel.getId())).thenReturn(Optional.of(pendaftarModel));
        assertEquals(pendaftarModel, pendaftarService.sortRekruitmen(pendaftarModel.getId(),1));
        assertEquals(pendaftarModel, pendaftarService.sortRekruitmen(pendaftarModel.getId(),2));
        assertNull(pendaftarService.sortRekruitmen(pendaftarModel.getId(), 3));
    }

    @Test
    void testSortRekruitmenReturnNull() {
        assertNull(pendaftarService.sortRekruitmen(pendaftarModel.getId(), 1));
    }
}
