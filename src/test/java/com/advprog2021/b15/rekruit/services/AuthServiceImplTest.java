package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.model.UserModelRole;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import com.advprog2021.b15.rekruit.repository.UserModelRepository;
import com.advprog2021.b15.rekruit.security.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {
    @Mock
    private UserModelRepository userModelRepository;

    @Mock
    private PendaftarModelRepository pendaftarModelRepository;

    @Mock
    private RekruterModelRepository rekruterModelRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AuthServiceImpl authService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @BeforeEach
    void setUp() {
        authService.setUserModelRepository(userModelRepository);
        authService.setPendaftarModelRepository(pendaftarModelRepository);
        authService.setRekruterModelRepository(rekruterModelRepository);
        authService.setPasswordEncoder(passwordEncoder);

    }

    @Test
    void testRegisterPendaftar() {
        when(userModelRepository.findByEmail("yeager.eren@survey.corps")).thenReturn(Optional.empty());

        PendaftarModel pendaftarModel = new PendaftarModel();
        pendaftarModel.setEmail("yeager.eren@survey.corps");
        pendaftarModel.setPassword("MIKASAAA");
        pendaftarModel.setUserRole(UserModelRole.PENDAFTAR);
        pendaftarModel.setId(1L);
        pendaftarModel.setFakultas("Pasukan Pengintai");
        pendaftarModel.setKontak("5164514");
        pendaftarModel.setNamaLengkap("Eren Yeager");
        pendaftarModel.setNpm("2006190699");

        when(authService.registerPendaftar(pendaftarModel)).thenReturn(pendaftarModel);

        PendaftarModel result = authService.registerPendaftar(pendaftarModel);

        assertEquals("yeager.eren@survey.corps", result.getEmail());

    }

    @Test
    void testRegisterRekruter() {
        when(userModelRepository.findByEmail("keith.sadies@cadet.corps")).thenReturn(Optional.empty());

        RekruterModel rekruterModel = new RekruterModel();
        rekruterModel.setEmail("keith.sadies@cadet.corps");
        rekruterModel.setPassword("aot-ending-sucks");
        rekruterModel.setUserRole(UserModelRole.REKRUTER);
        rekruterModel.setId(1L);
        rekruterModel.setNamaRekruter("Keith Sadies");
        rekruterModel.setDeskripsiRekruter("Cadet Selector");

        when(authService.registerRekruter(rekruterModel)).thenReturn(rekruterModel);

        RekruterModel result = authService.registerRekruter(rekruterModel);

        assertEquals("keith.sadies@cadet.corps", result.getEmail());
    }

    @Test
    void testRegisterButEmailExist() {
        RekruterModel rekruterModel = new RekruterModel();
        rekruterModel.setEmail("keith.sadies@cadet.corps");
        rekruterModel.setPassword("aot-ending-sucks");
        rekruterModel.setUserRole(UserModelRole.REKRUTER);
        rekruterModel.setId(1L);
        rekruterModel.setNamaRekruter("Keith Sadies");
        rekruterModel.setDeskripsiRekruter("Cadet Selector");

        when(userModelRepository.findByEmail("keith.sadies@cadet.corps")).thenReturn(Optional.of(rekruterModel));

        assertThrows(IllegalStateException.class, () -> {
            authService.registerRekruter(rekruterModel);
        });
    }
}
