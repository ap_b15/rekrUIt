package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.MendaftarModel;
import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.repository.MendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.PengumumanRepository;
import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RekruiterServiceImplTest {
    @Mock
    private RekruitmenModelRepository rekruitmenModelRepository;
    @Mock
    private RekruterModelRepository rekruterModelRepository;
    @Mock
    private PengumumanRepository pengumumanRepository;
    @Mock
    private MendaftarModelRepository mendaftarModelRepository;

    @InjectMocks
    private RekruiterServiceImpl rekruiterService;
    @InjectMocks
    private MendaftarServiceImpl mendaftarService;

    private RekruitmenModel rekruitmenModel;
    private RekruterModel rekruterModel;
    private PengumumanModel pengumumanModel;
    private MendaftarModel mendaftarModel;
    private Set<RekruitmenModel> rekruitmenModelSet = new HashSet<RekruitmenModel>();

    @BeforeEach
    public void setUp() {
        rekruterModel = new RekruterModel();
        rekruterModel.setId(1L);
        rekruterModel.setEmail("semogaadproA@gmail.com");
        rekruterModel.setPassword("aminpalingserius");
        rekruterModel.setNamaRekruter("Pacilian");
        rekruterModel.setDeskripsiRekruter("test doang kok");
        rekruterModel.setRekruitmenModelSet(Collections.emptySet());

        rekruitmenModel = new RekruitmenModel();
        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(rekruterModel);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");
        rekruitmenModel.setPengumumanModelSet(new HashSet<>());

        rekruitmenModelSet.add(rekruitmenModel);
        rekruterModel.setRekruitmenModelSet(rekruitmenModelSet);

        pengumumanModel = new PengumumanModel();
        pengumumanModel.setId(0);
        var waktu = Timestamp.valueOf(LocalDateTime.now()).getTime();
        pengumumanModel.setWaktu(waktu);
        pengumumanModel.setJudul("Update Tugas Video");
        pengumumanModel.setIsi("Tugas Video di extend yeay");

        mendaftarModel = new MendaftarModel();
        mendaftarModel.setId("19000000");
        mendaftarModel.setLinkCV("linkcv.com");
        mendaftarModel.setLinkTugas("linktugas.com");
        mendaftarModel.setStatusPenerimaan("Belum Diterima");
        mendaftarModel.setNilai(80);
        mendaftarModel.setPendaftarModel(null);
        mendaftarModel.setRekruitmenModel(null);
    }

    @Test
    void TestCreateRekruitmenReturnRekruitmen() {
        when(rekruterModelRepository.findById(1L)).thenReturn(rekruterModel);
        RekruitmenModel bucketCreatingModel = rekruiterService.createRekruitmen(1L, rekruitmenModel);
        assertEquals(rekruitmenModel.getId(), bucketCreatingModel.getId());
    }

    @Test
    void TestCreateRekruitmenReturnNull() {
        when(rekruterModelRepository.findById(1L)).thenReturn(rekruterModel);
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);

        assertNull(rekruiterService.createRekruitmen(1L, rekruitmenModel));
    }

    @Test
    void TestCreatePengumuman() {
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);
        PengumumanModel bucketPengumumanModel = rekruiterService.createPengumuman(pengumumanModel,  1L);

        assertEquals(pengumumanModel.getId(), bucketPengumumanModel.getId());
    }
    @Test
    void TestCreatePengumumanDoesNotExistReturnNull(){
        PengumumanModel bucketPengumumanModel = rekruiterService.createPengumuman(pengumumanModel,  10);
        assertNull(bucketPengumumanModel);
    }

    @Test
    void TestServicesGetListPengumuman(){
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);
        List<PengumumanModel> bucketPengumumanModel = rekruiterService.getListPengumumanModels(1L);

        assertEquals(new ArrayList<>(), bucketPengumumanModel);
    }

    @Test
    void TestServicesGetListPengumumanReturnNull(){
        when(rekruitmenModelRepository.findById(1L)).thenReturn(null);
        List<PengumumanModel> bucketPengumumanModel = rekruiterService.getListPengumumanModels(1L);

        assertEquals(new ArrayList<>(), bucketPengumumanModel);
    }

    @Test
    void TestServicesGetRekruitmenByID() {
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);
        RekruitmenModel bucketRekruitmenModel = rekruiterService.getRekruitmenModelByID(1L);

        assertEquals(rekruitmenModel.getJudul(), bucketRekruitmenModel.getJudul());
    }

    @Test
    void TestUpdateRekruitmenReturnRekruitmen() {
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);
        RekruitmenModel tempChange = new RekruitmenModel();
        tempChange = new RekruitmenModel();
        tempChange.setId(1L);
        tempChange.setJudul("Pencari Uang Pendaftar");
        tempChange.setDidaftarOleh(null);
        tempChange.setRekruiter(rekruterModel);
        tempChange.setStatus("Sudah Dibuka");
        tempChange.setNarahubung("081219447378");
        tempChange.setDeskripsiTugas("review risol yang enak");
        tempChange.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        tempChange.setSyaratKetentuan("mahasiswa pacil");
        tempChange.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        tempChange.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        tempChange.setLinkWawancara("www.google.com");

        RekruitmenModel bucketUpdatingModel = rekruiterService.updateRekruitmen(1L, tempChange);
        assertNotEquals(rekruitmenModel.getJudul(), bucketUpdatingModel.getJudul());
    }

    @Test
    void TestUpdateRekruitmenReturnNull() {
        when(rekruitmenModelRepository.findById(1L)).thenReturn(null);

        assertNull(rekruiterService.updateRekruitmen(1L, rekruitmenModel));
    }

    @Test
    void TestGetRekruitmenReturnRekruitmen() {
        when(rekruterModelRepository.findById(1L)).thenReturn(rekruterModel);
        List<RekruitmenModel> bucketRekruitmenModel = rekruiterService.getRekruitmenModels(1L);

        assertEquals(new ArrayList<>(rekruitmenModelSet), bucketRekruitmenModel);
    }

    @Test
    void TestGetRekruitmenReturnNull() {
        when(rekruterModelRepository.findById(1L)).thenReturn(null);

        assertEquals(new ArrayList<>(), rekruiterService.getRekruitmenModels(1L));
    }

    @Test
    void TestGetAllRekruitmenReturnAllRekruitmen() {
        when(rekruitmenModelRepository.findAll()).thenReturn(new ArrayList<>(rekruitmenModelSet));
        List<RekruitmenModel> bucketAllRekruitmenModel = rekruiterService.getAllRekruitmenModels();

        assertEquals(new ArrayList<>(rekruitmenModelSet), bucketAllRekruitmenModel);
    }

    @Test
    void TestStatusGeneratorifBelumDibuka() {
        Timestamp startDate = Timestamp.valueOf("2022-01-26 09:00:00");
        Timestamp dueDate = Timestamp.valueOf("2022-01-26 11:01:15");

        assertEquals("Belum Dibuka", rekruiterService.statusGenerator(startDate, dueDate));
    }

    @Test
    void TestStatusGeneratorifSudahDibuka() {
        Timestamp startDate = Timestamp.valueOf("2021-01-26 09:00:00");
        Timestamp dueDate = Timestamp.valueOf("2022-01-26 11:01:15");

        assertEquals("Sudah Dibuka", rekruiterService.statusGenerator(startDate, dueDate));
    }

    @Test
    void TestStatusGeneratorifSudahDitutup() {
        Timestamp startDate = Timestamp.valueOf("2021-01-26 09:00:00");
        Timestamp dueDate = Timestamp.valueOf("2021-01-26 11:01:15");

        assertEquals("Sudah Ditutup", rekruiterService.statusGenerator(startDate, dueDate));
    }

    @Test
    void TestGetRekruiterModel() {
        when(rekruterModelRepository.findById(1L)).thenReturn(rekruterModel);
        RekruterModel tempRekruterModel = rekruiterService.getRekruiterModel(1L);

        assertEquals(rekruterModel.getId(), tempRekruterModel.getId());
    }

    @Test
    void TestMenilaiRekruitmenReturnMendaftarModel() {
        when(mendaftarModelRepository.findById("1")).thenReturn(Optional.ofNullable(mendaftarModel));
        var TempMendaftarModel = mendaftarModelRepository.findById("1");

        assertTrue(TempMendaftarModel.isPresent());

        TempMendaftarModel.get().setStatusPenerimaan("Ditolak");
        TempMendaftarModel.get().setNilai(100);

        mendaftarService.updateNilaiMendaftar("asd", TempMendaftarModel.get());

        MendaftarModel bucketMendaftarModel = rekruiterService.menilaiRekruitmen("asd", "Ditolak", 100);
//        assertNotEquals(TempMendaftarModel.get().getId(), bucketMendaftarModel.getId());
    }

    @Test
    void TestMenilaiRekruitmenReturnNull() {
        MendaftarModel mendaftarModel2 = null;
        when(mendaftarModelRepository.findById("1")).thenReturn(Optional.ofNullable(mendaftarModel2));
        MendaftarModel tempMendaftarModel = rekruiterService.menilaiRekruitmen("1", "Belum Diterima", 80);

        assertNull(tempMendaftarModel);
    }

    @Test
    void TestDeleteRekruitmenBranchBerhasil() {
        RekruitmenModel optional;
        when(rekruterModelRepository.findById(1L)).thenReturn(rekruterModel);
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);

        assertEquals(rekruitmenModel.getRekruiter().getId(), rekruterModel.getId());

        rekruitmenModelRepository.save(rekruitmenModel);

        assertEquals("Berhasil", rekruiterService.deleteRekruitmen(1L, 1L));
    }

    @Test
    void TestDeleteRekruitmenBranchGagal() {
        RekruitmenModel optional;
        when(rekruterModelRepository.findById(1L)).thenReturn(null);
        when(rekruitmenModelRepository.findById(1L)).thenReturn(rekruitmenModel);

        rekruitmenModelRepository.save(rekruitmenModel);

        assertEquals("Gagal", rekruiterService.deleteRekruitmen(1L, 1L));
    }
}
