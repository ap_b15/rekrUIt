package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.repository.PengumumanRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PengumumanServiceImplTests {
    @Mock
    private RekruterModelRepository rekruterModelRepository;

    @Mock
    private PengumumanRepository pengumumanRepository;

    @InjectMocks
    private PengumumanServiceImpl pengumumanService;

    private PengumumanModel pengumumanModel;
    private RekruterModel rekruterModel;
    @BeforeEach
    public void setUp(){
        rekruterModel = new RekruterModel();
        rekruterModel.setNamaRekruter("OKK");
        rekruterModel.setDeskripsiRekruter("Okk ui");
        pengumumanModel = new PengumumanModel();
        pengumumanModel.setId(0L);
        pengumumanModel.setWaktu(1);
        pengumumanModel.setJudul("Update Tugas Video");
        pengumumanModel.setIsi("Tugas Video di extend yeay");
    }
    @Test
    void testServiceGetPengumumanByIdNotNull() {
        when(pengumumanRepository.findById(0)).thenReturn(pengumumanModel);
        PengumumanModel resultPengumuman = pengumumanService.getPengumumanById(pengumumanModel.getId());
        assertEquals(pengumumanModel.getId(), resultPengumuman.getId());
    }
    @Test
    void testServiceGetPengumumanByIdNull(){
        assertEquals(null, pengumumanService.getPengumumanById(pengumumanModel.getId()));
    }
    @Test
    void testServiceUpdatePengumumanReturnNotNull() {
        PengumumanModel currentPengumumanModel;
        when(pengumumanRepository.findById(0)).thenReturn(pengumumanModel);
        currentPengumumanModel = pengumumanService.updatePengumuman(pengumumanModel, pengumumanModel.getId());

        assertEquals(pengumumanModel, currentPengumumanModel);
    }
    @Test
    void testServiceUpdatePengumumanReturnNull() {
        assertEquals( null,pengumumanService.updatePengumuman(pengumumanModel, pengumumanModel.getId()));
    }
    @Test
    void testServiceDeletePengumumanDeleteFromRepository() {
        when(pengumumanRepository.findById(0)).thenReturn(pengumumanModel);
        pengumumanService.deletePengumuman(pengumumanModel.getId() );
        when(pengumumanRepository.findById(0)).thenReturn(null);

        assertEquals(null, pengumumanRepository.findById(0));

    }


}
