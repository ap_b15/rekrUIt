package com.advprog2021.b15.rekruit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class RekruterModelTest {

    private final RekruterModel rekruiter = new RekruterModel();

    @BeforeEach
    public void setUp() {
        rekruiter.setId(69L);
        rekruiter.setNamaRekruter("Hange Zoe");
        rekruiter.setDeskripsiRekruter("Komandan Pasukan Pengintai");
        rekruiter.setRekruitmenModelSet(null);
    }

    @Test
    void getIdShouldReturnLong() {
        assertEquals(69L, rekruiter.getId());
    }

    @Test
    void getNamaRekruterShouldReturnString() {
        assertEquals("Hange Zoe", rekruiter.getNamaRekruter());
    }

    @Test
    void getDeskripsiRekruterShouldReturnString() {
        assertEquals("Komandan Pasukan Pengintai", rekruiter.getDeskripsiRekruter());
    }

    @Test
    void getModelSetShouldReturnNull() {
        assertNull(rekruiter.getRekruitmenModelSet());
    }
}
