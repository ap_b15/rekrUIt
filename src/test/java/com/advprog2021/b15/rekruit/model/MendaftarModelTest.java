package com.advprog2021.b15.rekruit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)

class MendaftarModelTest {

    private MendaftarModel mendaftarModel = new MendaftarModel();
    private MendaftarModel mendaftarModel2 = new MendaftarModel("dimas", null, null, null, -1, null, null);

    @BeforeEach
    public void setUp() throws Exception{
        mendaftarModel.setId("19000000");
        mendaftarModel.setLinkCV("linkcv.com");
        mendaftarModel.setLinkTugas("linktugas.com");
        mendaftarModel.setStatusPenerimaan("Belum Diterima");
        mendaftarModel.setNilai(80);
        mendaftarModel.setPendaftarModel(null);
        mendaftarModel.setRekruitmenModel(null);

    }

    @Test
    void getIDShouldReturnString() {
        assertEquals("19000000", mendaftarModel.getId());
    }

    @Test
    void getLinkCVShouldReturnString() {
        assertEquals("linkcv.com", mendaftarModel.getLinkCV());
    }

    @Test
    void getLinkTugasShouldReturnString() {
        assertEquals("linktugas.com", mendaftarModel.getLinkTugas());
    }

    @Test
    void getStatusPenerimaanShouldReturnString() {
        assertEquals("Belum Diterima", mendaftarModel.getStatusPenerimaan());
    }

    @Test
    void getNilaiShouldReturnInt() {
        int nilai = 80;
        assertEquals(nilai, mendaftarModel.getNilai());
    }

    @Test
    void getPendaftarModelShouldReturnNull() {
        assertEquals(null, mendaftarModel.getPendaftarModel());
    }

    @Test
    void getRekruitmenModelShouldReturnNull() {
        assertEquals(null, mendaftarModel.getRekruitmenModel());
    }

    @Test
    void createMendaftarModelShouldGetModel() {
        assertEquals("dimas", mendaftarModel2.getId());
    }

}
