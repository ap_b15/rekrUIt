package com.advprog2021.b15.rekruit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PengumumanModelTests {


    private PengumumanModel pengumumanModel ;
    private RekruitmenModel rekruitmenModel = new RekruitmenModel();

    @BeforeEach
    void setUp() throws Exception{
        pengumumanModel = new PengumumanModel("Pengumuman Baru","pengumpulan tugas di exteend");

        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(null);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");
        rekruitmenModel.setPengumumanModelSet(null);

        pengumumanModel.setWaktu(1);
        pengumumanModel.setJudul("Pengumuman Baru");
        pengumumanModel.setIsi("pengumpulan tugas di exteend");
        pengumumanModel.setId(0);
        pengumumanModel.setRekruitmenModel(rekruitmenModel);

    }
    @Test
    void getWaktuShouldReturnLocalDate(){assertEquals(1,pengumumanModel.getWaktu());}
    @Test
    void getJudulShouldReturnString() {
        assertEquals( "Pengumuman Baru",pengumumanModel.getJudul());
    }
    @Test
    void getRekruitmenModelReturnRekruitmenModel(){
        assertEquals(pengumumanModel.getRekruitmenModel(), rekruitmenModel);
    }
    @Test
    void getIsiShouldReturnString(){
        assertEquals("pengumpulan tugas di exteend",pengumumanModel.getIsi());
    }
    @Test
    void getIdShouldReturnLong(){
        assertEquals(0,pengumumanModel.getId());
    }

}
