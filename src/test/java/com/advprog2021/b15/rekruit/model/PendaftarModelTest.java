package com.advprog2021.b15.rekruit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)

class PendaftarModelTest {

    private PendaftarModel pendaftarModel = new PendaftarModel();

    @BeforeEach
    public void setUp() {
        pendaftarModel.setEmail("Dummy@Gmail.com");
        pendaftarModel.setPassword("dummyPassword");
        pendaftarModel.setMendaftarRekruitmen(null);
        pendaftarModel.setNamaLengkap("Dummy");
        pendaftarModel.setNpm("19000000");
        pendaftarModel.setFakultas("Fasilkom");
        pendaftarModel.setKontak("dummy21");
    }

    @Test
    void getEmailShouldReturnString() {
        assertEquals("Dummy@Gmail.com", pendaftarModel.getUsername());
    }

    @Test
    void getPasswordShouldReturnString() {
        assertEquals("dummyPassword", pendaftarModel.getPassword());
    }

    @Test
    void getNamaLengkapShouldReturnString() {
        assertEquals("Dummy", pendaftarModel.getNamaLengkap());
    }

    @Test
    void getNpmShouldReturngString() {
        assertEquals("19000000", pendaftarModel.getNpm());
    }

    @Test
    void getFakultasShouldReturnString() {
        assertEquals("Fasilkom", pendaftarModel.getFakultas());
    }

    @Test
    void getKontakShouldReturnString() {
        assertEquals("dummy21",pendaftarModel.getKontak());
    }

    @Test
    void getMendaftarRekruitmenShouldReturnNull() {
        assertEquals(null, pendaftarModel.getMendaftarRekruitmen());
    }

}
