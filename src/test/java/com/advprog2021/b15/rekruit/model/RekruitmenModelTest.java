package com.advprog2021.b15.rekruit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RekruitmenModelTest {

    private final RekruitmenModel rekruitmenModel = new RekruitmenModel();

    @BeforeEach
    public void setUp() {
        rekruitmenModel.setId(1L);
        rekruitmenModel.setJudul("Pencari Uang Panitia");
        rekruitmenModel.setDidaftarOleh(null);
        rekruitmenModel.setRekruiter(null);
        rekruitmenModel.setStatus("Sudah Dibuka");
        rekruitmenModel.setNarahubung("081219447378");
        rekruitmenModel.setDeskripsiTugas("review risol yang enak");
        rekruitmenModel.setDeskripsiPekerjaan("jualan rison ke mahasiswa");
        rekruitmenModel.setSyaratKetentuan("mahasiswa pacil");
        rekruitmenModel.setStartDateRegistrasi(Timestamp.valueOf("2021-01-26 09:00:00"));
        rekruitmenModel.setDueDateRegistrasi(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setDueDateTugas(Timestamp.valueOf("2021-01-26 11:01:15"));
        rekruitmenModel.setLinkWawancara("www.google.com");
        rekruitmenModel.setPengumumanModelSet(null);
    }

    @Test
    void getIdShouldReturnLong() {assertEquals(1L, rekruitmenModel.getId());}

    @Test
    void getJudulShouldReturnString() {
        assertEquals("Pencari Uang Panitia",rekruitmenModel.getJudul());
    }

    @Test
    void getStatusShouldReturnString() {
        assertEquals("Sudah Dibuka", rekruitmenModel.getStatus());
    }

    @Test
    void getNarahubungShouldReturnString() {
        assertEquals( "081219447378", rekruitmenModel.getNarahubung());
    }

    @Test
    void getDeskripsi_tugasShouldReturnString() {
        assertEquals("review risol yang enak", rekruitmenModel.getDeskripsiTugas());
    }

    @Test
    void getDeskripsi_pekerjaanShouldReturnString() {
        assertEquals("jualan rison ke mahasiswa", rekruitmenModel.getDeskripsiPekerjaan());
    }

    @Test
    void getSyarat_ketentuanShouldReturnString() {
        assertEquals("mahasiswa pacil", rekruitmenModel.getSyaratKetentuan());
    }

    @Test
    void getLink_wawancaraShouldReturnString() {
        assertEquals("www.google.com", rekruitmenModel.getLinkWawancara());
    }

    @Test
    void getDidaftar_olehShouldReturnNull() {
        assertNull(rekruitmenModel.getDidaftarOleh());
    }

    @Test
    void getRekruiterShouldReturnNull() {
        assertNull(rekruitmenModel.getRekruiter());
    }

    @Test
    void getStart_date_registrasiShouldReturnTimeStamp() {
        assertEquals(Timestamp.valueOf("2021-01-26 09:00:00"), rekruitmenModel.getStartDateRegistrasi());
    }

    @Test
    void getDue_date_registrasiShouldReturnTimeStamp() {
        assertEquals(Timestamp.valueOf("2021-01-26 11:01:15"), rekruitmenModel.getDueDateRegistrasi());
    }

    @Test
    void getDue_date_tugasShouldReturnTimeStamp() {
        assertEquals(Timestamp.valueOf("2021-01-26 11:01:15"), rekruitmenModel.getDueDateTugas());
    }

    @Test
    void getPengumumanModelSetSHouldReturnNull() { assertNull(rekruitmenModel.getPengumumanModelSet());}
}
