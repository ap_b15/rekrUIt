package com.advprog2021.b15.rekruit.utils;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class NameSorter implements Sorter {
    @Override
    public PendaftarModel filterAction(PendaftarModel pendaftarModel) {
        ArrayList<RekruitmenModel> dataRekruitmen = new ArrayList<>();
        dataRekruitmen.addAll(pendaftarModel.getMendaftarRekruitmen());
        for (var i = 0 ; i < dataRekruitmen.size() ; i++) {
            for (var j = 0 ; j < i ; j++ ) {
                if (dataRekruitmen.get(i).getJudul().compareTo(dataRekruitmen.get(j).getJudul())<0) {
                    RekruitmenModel temp = dataRekruitmen.get(i);
                    dataRekruitmen.set(i, dataRekruitmen.get(j));
                    dataRekruitmen.set(j, temp);
                }
            }
        }
        Set<RekruitmenModel> sortedData = new LinkedHashSet<>(dataRekruitmen);
        pendaftarModel.setMendaftarRekruitmen(sortedData);
        return pendaftarModel;
    }
}
