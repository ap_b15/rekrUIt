package com.advprog2021.b15.rekruit.utils;


import com.advprog2021.b15.rekruit.model.PendaftarModel;

public interface Sorter {
    PendaftarModel filterAction(PendaftarModel pendaftarModel);
}
