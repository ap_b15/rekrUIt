package com.advprog2021.b15.rekruit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RekrUItApplication {

	public static void main(String[] args) {
		SpringApplication.run(RekrUItApplication.class, args);
	}

}
