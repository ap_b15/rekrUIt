package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PendaftarModel;

public interface PendaftarService {
    PendaftarModel getPendaftarById(long idPendaftar);
    public PendaftarModel updateDataPendaftar(long idPendaftar, PendaftarModel pendaftarModel);
    public String update(String message);
    public  void mendaftarRekruitmen(long idRekruitmen, long idPendaftar);
    public  PendaftarModel sortRekruitmen(long idPendaftar, long kode);
}
