package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;

public interface AuthService {

    PendaftarModel registerPendaftar(PendaftarModel pendaftar);
    RekruterModel registerRekruter(RekruterModel rekruter);

}
