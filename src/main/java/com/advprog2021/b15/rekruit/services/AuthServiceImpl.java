package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.model.UserModel;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import com.advprog2021.b15.rekruit.repository.UserModelRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Setter
public class AuthServiceImpl implements AuthService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthServiceImpl(
            UserModelRepository userModelRepository,
            PasswordEncoder passwordEncoder) {
        this.userModelRepository = userModelRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    private PendaftarModelRepository pendaftarModelRepository;

    @Autowired
    private RekruterModelRepository rekruterModelRepository;

    @Override
    public PendaftarModel registerPendaftar(PendaftarModel pendaftar) {
        registrationHelper(pendaftar, userModelRepository);
        return pendaftarModelRepository.save(pendaftar);
    }

    @Override
    public RekruterModel registerRekruter(RekruterModel rekruter) {
        registrationHelper(rekruter, userModelRepository);
        return rekruterModelRepository.save(rekruter);
    }

    public void registrationHelper(UserModel model, UserModelRepository repository) {
        boolean userExists = repository.findByEmail(model.getEmail()).isPresent();

        if (userExists) {
            throw new IllegalStateException("email already taken");
        }

        String encodedPassword = passwordEncoder.encode(model.getPassword());
        model.setPassword(encodedPassword);
    }
}
