package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.*;
import com.advprog2021.b15.rekruit.repository.MendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.PengumumanRepository;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class RekruiterServiceImpl implements RekruiterService {

    @Autowired
    private RekruitmenModelRepository rekruitmenModelRepository;

    @Autowired
    private RekruterModelRepository rekruterModelRepository;

    @Autowired
    private PengumumanRepository pengumumanRepository;

    @Autowired
    private MendaftarModelRepository mendaftarModelRepository;

    @Autowired
    private MendaftarService mendaftarService;

    @Override
    public RekruitmenModel createRekruitmen(long idRekruiter, RekruitmenModel rekruitmenModel) {
        var rekruterModel = rekruterModelRepository.findById(idRekruiter);
        var currentRekruitmenModel =rekruitmenModelRepository.findById(rekruitmenModel.getId());

//      Sprint 2 : Implement Edge Case Handler --> rekruterModel auth
        if (currentRekruitmenModel == null) {
            rekruitmenModel.setRekruiter(rekruterModel);
            rekruitmenModelRepository.save(rekruitmenModel);

            return rekruitmenModel;
        }

        return null;
    }

    @Override
    public RekruitmenModel updateRekruitmen(long idRekruitmen, RekruitmenModel rekruitmenModel) {
        var currentRekruitmenModel = rekruitmenModelRepository.findById(idRekruitmen);

//      Sprint 2 : Implement Edge Case Handler --> rekruterModel auth
        if (currentRekruitmenModel != null) {
            rekruitmenModel.setId(currentRekruitmenModel.getId());
            rekruitmenModel.setRekruiter(currentRekruitmenModel.getRekruiter());
            rekruitmenModel.setDidaftarOleh(currentRekruitmenModel.getDidaftarOleh());
            rekruitmenModelRepository.delete(currentRekruitmenModel);
            rekruitmenModelRepository.save(rekruitmenModel);

            return rekruitmenModel;
        }

        return null;
    }

    @Override
    public PengumumanModel createPengumuman(PengumumanModel pengumumanModel,  long idRekruitmen){
        var rekruitmenModel = rekruitmenModelRepository.findById(idRekruitmen);

        pengumumanModel.setRekruitmenModel(rekruitmenModel);

        if (rekruitmenModel != null) {
            pengumumanModel.setRekruitmenModel(rekruitmenModel);
            Set<PengumumanModel> pengumumanModelSet = rekruitmenModel.getPengumumanModelSet();
            pengumumanModelSet.add(pengumumanModel);
            rekruitmenModel.setPengumumanModelSet(pengumumanModelSet);
            long waktu = Timestamp.valueOf(LocalDateTime.now()).getTime();
            pengumumanModel.setWaktu(waktu);
            pengumumanRepository.save(pengumumanModel);

            return pengumumanModel;
        }

        return null;
    }

    @Override
    public List<PengumumanModel> getListPengumumanModels(long idRekruitmen) {
        var rekruitmenModel = rekruitmenModelRepository.findById(idRekruitmen);

        if (rekruitmenModel != null) {
            Set<PengumumanModel> pengumumanModelSet = rekruitmenModel.getPengumumanModelSet();

            return new ArrayList<>(pengumumanModelSet);
        }

        return new ArrayList<>();
    }

    @Override
    public List<RekruitmenModel> getRekruitmenModels(long idRekruiter) {
        var rekruterModel = rekruterModelRepository.findById(idRekruiter);

//      Sprint 2 : Implement Edge Case Handler --> rekruiterModel auth
        if (rekruterModel != null) {
            Set<RekruitmenModel> rekruitmenModelSet = rekruterModel.getRekruitmenModelSet();

            for (RekruitmenModel bucketRekruitmenMode : rekruitmenModelSet) {
                String currentStatus = statusGenerator(bucketRekruitmenMode.getStartDateRegistrasi(), bucketRekruitmenMode.getDueDateRegistrasi());
                bucketRekruitmenMode.setStatus(currentStatus);
            }

            return new ArrayList<>(rekruitmenModelSet);
        }

        return new ArrayList<>();
    }

    @Override
    public RekruitmenModel getRekruitmenModelByID(long idRekruitmen) {
        return rekruitmenModelRepository.findById(idRekruitmen);
    }

    @Override
    public List<RekruitmenModel> getAllRekruitmenModels() {
        var allRekruitmen = rekruitmenModelRepository.findAll();

        for (RekruitmenModel bucketRekruitmenMode : allRekruitmen) {
            String currentStatus = statusGenerator(bucketRekruitmenMode.getStartDateRegistrasi(), bucketRekruitmenMode.getDueDateRegistrasi());
            bucketRekruitmenMode.setStatus(currentStatus);
        }
        return allRekruitmen;
    }

    @Override
    public String statusGenerator(Timestamp startTimeStamp, Timestamp dueTimeStamp) {
        var currentTimeStamp = new Timestamp(System.currentTimeMillis());

        if (currentTimeStamp.getTime() >= startTimeStamp.getTime()) {

            if (currentTimeStamp.getTime() < dueTimeStamp.getTime()) {
                return "Sudah Dibuka";
            }
            return "Sudah Ditutup";
        }
        return "Belum Dibuka";
    }

    @Override
    public RekruterModel getRekruiterModel(long idRekruiter) {
        return rekruterModelRepository.findById(idRekruiter);
    }

    @Override
    public MendaftarModel menilaiRekruitmen(String idMendaftar, String statusPenerimaan, int nilaiPenerimaan) {
        var mendaftarModel = mendaftarModelRepository.findById(idMendaftar);

        if (mendaftarModel.isPresent()) {
            mendaftarModel.get().setStatusPenerimaan(statusPenerimaan);
            mendaftarModel.get().setNilai(nilaiPenerimaan);

            mendaftarService.updateNilaiMendaftar(idMendaftar, mendaftarModel.get());

            return mendaftarModel.get();
        }

        return null;
    }

    @Override
    public String deleteRekruitmen(long idRekruiter, long idRekruitmen) {
        var rekruterModel = rekruterModelRepository.findById(idRekruiter);
        var currentRekruitmenModel = rekruitmenModelRepository.findById(idRekruitmen);

        if (rekruterModel != null && currentRekruitmenModel != null && currentRekruitmenModel.getRekruiter().getId() == rekruterModel.getId()) {
            rekruitmenModelRepository.deleteById(idRekruitmen);

            return "Berhasil";
        }

        return "Gagal";
    }
}
