package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PengumumanModel;


public interface PengumumanService {

    PengumumanModel getPengumumanById(long id);

    PengumumanModel updatePengumuman(PengumumanModel pengumumanModel, long id);

    void deletePengumuman(long id);
}
