package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.model.*;

import java.sql.Timestamp;
import java.util.List;

public interface RekruiterService {

    RekruitmenModel createRekruitmen(long idRekruiter, RekruitmenModel rekruitmenModel);

    RekruitmenModel updateRekruitmen(long idRekruitmen, RekruitmenModel rekruitmenModel);

    PengumumanModel createPengumuman(PengumumanModel pengumumanModel, long idRekruitmen);

    List<PengumumanModel> getListPengumumanModels(long idRekruitmen);

    List<RekruitmenModel> getRekruitmenModels(long idRekruiter);

    RekruitmenModel getRekruitmenModelByID(long idRekruitmen);

    List<RekruitmenModel> getAllRekruitmenModels();

    String statusGenerator(Timestamp startTimeStamp, Timestamp dueTimeStamp);

    RekruterModel getRekruiterModel(long idRekruiter);

    MendaftarModel menilaiRekruitmen(String idMendaftar, String statusPenerimaan, int nilaiPenerimaan);

    String deleteRekruitmen(long idRekruiter, long idRekruitmen);
}
