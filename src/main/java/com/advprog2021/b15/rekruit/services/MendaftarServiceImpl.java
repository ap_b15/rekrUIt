package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.MendaftarModel;
import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.repository.MendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;
import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

@Service
public class MendaftarServiceImpl implements MendaftarService {

    @Autowired
    private PendaftarModelRepository pendaftarModelRepository;

    @Autowired
    private MendaftarModelRepository mendaftarModelRepository;

    @Autowired
    private RekruitmenModelRepository rekruitmenModelRepository;

    @Autowired
    private PendaftarService pendaftarService;


    @Override
    public MendaftarModel postMendaftar(long idPendaftar, long id) {

        Optional<PendaftarModel> optionalPendaftar = pendaftarModelRepository.findById(idPendaftar);
        var rekruitmenModel = rekruitmenModelRepository.findById(id);

        if(optionalPendaftar.isPresent() && rekruitmenModel != null) {
            String idmendaftar = String.valueOf(idPendaftar) + "-" + id;
            var pendaftarModel = optionalPendaftar.get();
            var mendaftarModel = new MendaftarModel(idmendaftar, pendaftarModel, rekruitmenModel,null,-1, null, null);
            mendaftarModelRepository.save(mendaftarModel);
            return mendaftarModel;
       }
        return null;
    }

    @Override
    public MendaftarModel deleteMendaftar(long idPendaftar, String id) {
        String idmendaftar = String.valueOf(idPendaftar) + "-" + id;
        MendaftarModel mendaftarModel = null;
        Optional<MendaftarModel> optionalMendaftar = mendaftarModelRepository.findById(idmendaftar);

        if(optionalMendaftar.isPresent()){
            mendaftarModel = optionalMendaftar.get();
        }

        if(mendaftarModel != null) {
            var idRekruitmen = Long.parseLong(id);
            var pendaftar = pendaftarModelRepository.findById(idPendaftar);
            var rekruitmen = rekruitmenModelRepository.findById(idRekruitmen);

            Set<RekruitmenModel> listDidaftar;

            if(pendaftar.isPresent()) {
                listDidaftar = pendaftar.get().getMendaftarRekruitmen();
                listDidaftar.remove(rekruitmen);
                pendaftar.get().setMendaftarRekruitmen(listDidaftar);
                Set<PendaftarModel> listTerdaftar = rekruitmen.getDidaftarOleh();
                listTerdaftar.remove(pendaftar.get());
                rekruitmen.setDidaftarOleh(listTerdaftar);
            }

            mendaftarModelRepository.delete(mendaftarModel);

        }

        return null;
    }

    @Override
    public MendaftarModel updateLinkMendaftar(String id, MendaftarModel mendaftarModel) {
        var dataLama = mendaftarModelRepository.findById(id);
        if (dataLama.isPresent()) {
            dataLama.get().setLinkCV(mendaftarModel.getLinkCV());
            dataLama.get().setLinkTugas(mendaftarModel.getLinkTugas());
            mendaftarModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return  null;
        }
    }

    @Override
    public MendaftarModel updateNilaiMendaftar(String id, MendaftarModel mendaftarModel) {
        var dataLama = mendaftarModelRepository.findById(id);
        if (dataLama.isPresent()) {
            dataLama.get().setStatusPenerimaan(mendaftarModel.getStatusPenerimaan());
            dataLama.get().setNilai(mendaftarModel.getNilai());
            mendaftarModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return  null;
        }
    }

    @Override
    public MendaftarModel getMendaftarById(String idMendaftar) {
        var dataMendaftar = mendaftarModelRepository.findById(idMendaftar);
        return dataMendaftar.orElse(null);
    }

    @Override
    public ArrayList<MendaftarModel> getMendaftarByPartId(String idPartMendaftar) {
        var allDataMendaftar = mendaftarModelRepository.findAll();
        var result = new ArrayList<MendaftarModel>();
        for(MendaftarModel data : allDataMendaftar){
            if(data.getId().contains(idPartMendaftar)){
                result.add(data);
            }
        }
        return result;
    }
}
