package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.repository.PendaftarModelRepository;

import com.advprog2021.b15.rekruit.repository.RekruitmenModelRepository;
import com.advprog2021.b15.rekruit.utils.DueDateSorter;
import com.advprog2021.b15.rekruit.utils.NameSorter;
import com.advprog2021.b15.rekruit.utils.Sorter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PendaftarServiceImpl implements PendaftarService {

    @Autowired
    private PendaftarModelRepository pendaftarRepository;

    @Autowired
    private RekruitmenModelRepository rekruitmenRepository;

    @Override
    public PendaftarModel getPendaftarById(long idPendaftar) {
        var dataPendaftar = pendaftarRepository.findById(idPendaftar);
        return dataPendaftar.orElse(null);
    }

    @Override
    public PendaftarModel updateDataPendaftar(long idPendaftar, PendaftarModel pendaftarModel) {
        var dataLama = pendaftarRepository.findById(idPendaftar);
        if (dataLama.isPresent()) {
            dataLama.get().setNamaLengkap(pendaftarModel.getNamaLengkap());
            dataLama.get().setFakultas(pendaftarModel.getFakultas());
            dataLama.get().setKontak(pendaftarModel.getKontak());
            pendaftarRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return  null;
        }
    }

    @Override
    public String update(String message) {
        return message;
    }

    @Override
    public void mendaftarRekruitmen(long idRekruitmen, long idPendaftar) {
        var pendaftar = pendaftarRepository.findById(idPendaftar);
        var rekruitmen = rekruitmenRepository.findById(idRekruitmen);
        if(pendaftar.isPresent() && rekruitmen != null) {
            Set<RekruitmenModel> listDidaftar = pendaftar.get().getMendaftarRekruitmen();
            listDidaftar.add(rekruitmen);
            pendaftar.get().setMendaftarRekruitmen(listDidaftar);

            PendaftarModel isiPendaftar = pendaftar.get();
            Set<PendaftarModel> listTerdaftar = rekruitmen.getDidaftarOleh();
            listTerdaftar.add(isiPendaftar);
            rekruitmen.setDidaftarOleh(listTerdaftar);

        }
    }

    @Override
    public PendaftarModel sortRekruitmen(long idPendaftar, long kode) {
        var pendaftar = pendaftarRepository.findById(idPendaftar);
        if(pendaftar.isPresent()) {
            if (kode == 1) {
                Sorter nameSorter = new NameSorter();
                return nameSorter.filterAction(pendaftar.get());
            } else if (kode == 2) {
                Sorter dueDateSorter = new DueDateSorter();
                return dueDateSorter.filterAction(pendaftar.get());
            } else {
                return  null;
            }
        } else {
            return null;
        }
    }
}
