package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.repository.PengumumanRepository;
import com.advprog2021.b15.rekruit.repository.RekruterModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PengumumanServiceImpl implements PengumumanService{
    @Autowired
    private PengumumanRepository pengumumanRepository;

    @Autowired
    private RekruterModelRepository rekruterModelRepository;

    @Override
    public PengumumanModel getPengumumanById(long id){
        return pengumumanRepository.findById(id);
    }

    @Override
    public PengumumanModel updatePengumuman(PengumumanModel pengumumanModel, long id){
        var currentPengumumanModel = this.getPengumumanById(id);
        if(currentPengumumanModel==null){
            return null;
        }
        pengumumanModel.setRekruitmenModel(currentPengumumanModel.getRekruitmenModel());
        pengumumanModel.setId(id);
        pengumumanRepository.save(pengumumanModel);
        return pengumumanModel;
    }

    @Override
    public void deletePengumuman(long id){
        pengumumanRepository.deleteById(id);
    }
}
