package com.advprog2021.b15.rekruit.services;

import com.advprog2021.b15.rekruit.model.MendaftarModel;

import java.util.ArrayList;

public interface MendaftarService {
    MendaftarModel postMendaftar(long idPendaftar, long id);
    MendaftarModel deleteMendaftar(long idPendaftar, String id);
    MendaftarModel updateLinkMendaftar(String id, MendaftarModel mendaftarModel);
    MendaftarModel updateNilaiMendaftar(String id, MendaftarModel mendaftarModel);
    MendaftarModel getMendaftarById(String idMendaftar);
    ArrayList<MendaftarModel> getMendaftarByPartId(String idPartMendaftar);
}
