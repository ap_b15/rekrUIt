package com.advprog2021.b15.rekruit.repository;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PendaftarModelRepository extends JpaRepository<PendaftarModel, Long> {

    Optional<PendaftarModel> findById(long idPendaftar);

}
