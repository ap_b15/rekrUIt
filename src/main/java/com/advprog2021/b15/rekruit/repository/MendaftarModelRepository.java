package com.advprog2021.b15.rekruit.repository;

import com.advprog2021.b15.rekruit.model.MendaftarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface MendaftarModelRepository extends JpaRepository<MendaftarModel, String> {
    Optional<MendaftarModel> findById(String id);
}
