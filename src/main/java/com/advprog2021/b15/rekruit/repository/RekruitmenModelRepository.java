package com.advprog2021.b15.rekruit.repository;

import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RekruitmenModelRepository extends JpaRepository<RekruitmenModel, Long> {

    RekruitmenModel findById(long id);

}
