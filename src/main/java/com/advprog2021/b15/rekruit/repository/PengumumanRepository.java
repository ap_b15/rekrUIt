package com.advprog2021.b15.rekruit.repository;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PengumumanRepository extends JpaRepository<PengumumanModel, Long> {
    PengumumanModel findById(long id);

}
