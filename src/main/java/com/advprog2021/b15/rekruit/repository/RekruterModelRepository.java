package com.advprog2021.b15.rekruit.repository;

import com.advprog2021.b15.rekruit.model.RekruterModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RekruterModelRepository extends JpaRepository<RekruterModel, Long> {

    RekruterModel findById(long idRekruter);

}
