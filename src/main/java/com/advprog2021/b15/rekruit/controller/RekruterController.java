package com.advprog2021.b15.rekruit.controller;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.model.RekruitmenModel;
import com.advprog2021.b15.rekruit.services.MendaftarService;
import com.advprog2021.b15.rekruit.services.PengumumanService;
import com.advprog2021.b15.rekruit.services.RekruiterService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rekruter")
@AllArgsConstructor
public class RekruterController {

    @Autowired
    RekruiterService rekruiterService;

    @Autowired
    PengumumanService pengumumanService;

    @Autowired
    MendaftarService mendaftarService;

    @GetMapping(path = "/{idRekruiter}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getRekruitmen(@PathVariable(value = "idRekruiter") long idRekruiter) {
        return ResponseEntity.ok(rekruiterService.getRekruitmenModels(idRekruiter));
    }

    @GetMapping(path = "/rekruitmen", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getRekruitmen() {
        return ResponseEntity.ok(rekruiterService.getAllRekruitmenModels());
    }

    @PostMapping(path = "/{idRekruiter}", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> postRekruitmen(@RequestBody RekruitmenModel rekruitmenModel , @PathVariable(value = "idRekruiter") long idRekruiter) {
        return ResponseEntity.ok(rekruiterService.createRekruitmen(idRekruiter, rekruitmenModel));
    }

    @PutMapping(path = "/{idRekruitmen}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateRekruitmen(@RequestBody RekruitmenModel rekruitmenModel, @PathVariable(value = "idRekruitmen") long idRekruitmen) {
        return ResponseEntity.ok(rekruiterService.updateRekruitmen(idRekruitmen, rekruitmenModel));
    }
    @PostMapping(path = "/pengumuman/{idRekruitmen}", produces = {"application/json"})
    @ResponseBody
    public  ResponseEntity<Object> postPengumuman( @PathVariable(value = "idRekruitmen") long idRekruitmen, @RequestBody PengumumanModel pengumumanModel){
        return ResponseEntity.ok(rekruiterService.createPengumuman(pengumumanModel,  idRekruitmen));
    }

    @GetMapping(path = "/get/{idRekruiter}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getRekruiterModel(@PathVariable(value = "idRekruiter") long idRekruiter) {
        return ResponseEntity.ok(rekruiterService.getRekruiterModel(idRekruiter));
    }

    @PostMapping(path = "/menilai/{idMendaftar}/{statusPenerimaan}/{nilaiPenerimaan}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> menilaiRekruitmen(@PathVariable(value = "idMendaftar") String idMendaftar, @PathVariable(value = "statusPenerimaan") String statusPenerimaan, @PathVariable(value = "nilaiPenerimaan") int nilaiPenerimaan) {
        return ResponseEntity.ok(rekruiterService.menilaiRekruitmen(idMendaftar, statusPenerimaan, nilaiPenerimaan));
    }

    @GetMapping(path = "/rekruitmen/{idRekruitmen}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getRekruitmenByID(@PathVariable(value = "idRekruitmen") long idRekruitmen) {
        return ResponseEntity.ok(rekruiterService.getRekruitmenModelByID(idRekruitmen));
    }

    @DeleteMapping(path = "/rekruitmen/{idRekruiter}/{idRekruitmen}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> deleteRekruitmen(@PathVariable(value = "idRekruiter") long idRekruiter, @PathVariable(value = "idRekruitmen") long idRekruitmen) {
        return ResponseEntity.ok(rekruiterService.deleteRekruitmen(idRekruiter, idRekruitmen));
    }

    @GetMapping(path = "/mendaftar/{idPendaftar}/{idRekruitmen}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getMendaftarRekruter(@PathVariable(value = "idPendaftar") long idPendaftar, @PathVariable(value = "idRekruitmen") String idRekruitmen) {
        String idMendaftar = idPendaftar + "-" + idRekruitmen;
        return ResponseEntity.ok(mendaftarService.getMendaftarById(idMendaftar));
    }

    @GetMapping(path = "/mendaftar/{idRekruitmen}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getMendaftarRekruterKhusus(@PathVariable(value = "idRekruitmen") String idRekruitmen) {
        String idPartMendaftar = "-" + idRekruitmen;
        return ResponseEntity.ok(mendaftarService.getMendaftarByPartId(idPartMendaftar));
    }
}
