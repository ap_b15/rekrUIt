package com.advprog2021.b15.rekruit.controller;

import com.advprog2021.b15.rekruit.model.PengumumanModel;
import com.advprog2021.b15.rekruit.services.PengumumanService;
import com.advprog2021.b15.rekruit.services.RekruiterService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/pengumuman")
@AllArgsConstructor
public class PengumumanController {
    @Autowired
    private PengumumanService pengumumanService;

    @Autowired
    private RekruiterService rekruiterService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPengumumanById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(pengumumanService.getPengumumanById(id));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updatePengumuman(@RequestBody PengumumanModel pengumumanModel, @PathVariable(value = "id") long id) {
        return ResponseEntity.ok(pengumumanService.updatePengumuman(pengumumanModel, id));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<Object> deletePengumuman(@PathVariable(value = "id") long id) {
        pengumumanService.deletePengumuman(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/rekruitmen/{idRekruitmen}", produces = {"application/json"})
    public ResponseEntity<Object> getPengumumanByIdRekruitmen(@PathVariable(value = "idRekruitmen") long idRekruitmen) {
        return ResponseEntity.ok(rekruiterService.getListPengumumanModels(idRekruitmen));
    }
}
