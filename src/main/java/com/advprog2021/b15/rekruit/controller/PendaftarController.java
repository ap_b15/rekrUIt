package com.advprog2021.b15.rekruit.controller;


import com.advprog2021.b15.rekruit.model.MendaftarModel;
import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.services.MendaftarServiceImpl;
import com.advprog2021.b15.rekruit.services.PendaftarServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pendaftar")
@AllArgsConstructor
public class PendaftarController {

    @Autowired
    private PendaftarServiceImpl pendaftarServiceImpl;
    @Autowired
    private MendaftarServiceImpl mendaftarServiceImpl;

    @GetMapping(path = "/{idPendaftar}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPendaftar(@PathVariable(value = "idPendaftar") long idPendaftar) {
        return ResponseEntity.ok(pendaftarServiceImpl.getPendaftarById(idPendaftar));
    }

    @PutMapping(path = "/{idPendaftar}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updatePendaftar(@PathVariable(value = "idPendaftar") long idPendaftar, @RequestBody PendaftarModel pendaftarModel) {
        return ResponseEntity.ok(pendaftarServiceImpl.updateDataPendaftar(idPendaftar, pendaftarModel));
    }

    @PostMapping(path = "/{idPendaftar}/{id}", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> postDaftarRekruitmen(@RequestBody(required = false)  MendaftarModel mendaftar, @PathVariable(value = "idPendaftar") long idPendaftar, @PathVariable(value = "id") long id) {
        pendaftarServiceImpl.mendaftarRekruitmen(id,idPendaftar);
        return ResponseEntity.ok(mendaftarServiceImpl.postMendaftar(idPendaftar, id));
    }


    @DeleteMapping(path = "/{idPendaftar}/{id}", produces = {"application/json"})
    public ResponseEntity<Object> deleteDaftarRekuitmen(@PathVariable(value = "idPendaftar") long idPendaftar, @PathVariable(value = "id") String id) {
        mendaftarServiceImpl.deleteMendaftar(idPendaftar, id);
        return ResponseEntity.ok(mendaftarServiceImpl.deleteMendaftar(idPendaftar, id));
    }

    @PutMapping(path ="/mendaftar/{idMendaftar}", produces = {"application/json"})
    public ResponseEntity<Object>  updateMendaftar(@PathVariable(value = "idMendaftar") String idMendaftar, @RequestBody MendaftarModel mendaftarModel) {
        return ResponseEntity.ok(mendaftarServiceImpl.updateLinkMendaftar(idMendaftar, mendaftarModel));
    }

    @PostMapping(path = "/sort/{idPendaftar}/{kode}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> sortRekruitmen(@PathVariable(value = "idPendaftar") long idPendaftar, @PathVariable(value = "kode") long kode) {
        return ResponseEntity.ok(pendaftarServiceImpl.sortRekruitmen(idPendaftar,kode));
    }

    @GetMapping(path = "/mendaftar/{idPendaftar}/{idRekruitmen}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getMendaftarPendaftar(@PathVariable(value = "idPendaftar") long idPendaftar, @PathVariable(value = "idRekruitmen") String idRekruitmen) {
        String idMendaftar = idPendaftar + "-" + idRekruitmen;
        return ResponseEntity.ok(mendaftarServiceImpl.getMendaftarById(idMendaftar));
    }
}
