package com.advprog2021.b15.rekruit.controller;

import com.advprog2021.b15.rekruit.model.PendaftarModel;
import com.advprog2021.b15.rekruit.model.RekruterModel;
import com.advprog2021.b15.rekruit.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/auth")
@AllArgsConstructor
public class AuthController {

    @Autowired
    private AuthService authenticationService;

    @PostMapping(path = "/register/pendaftar")
    public ResponseEntity<Object> registerPendaftar(@RequestBody PendaftarModel pendaftar) {
        return ResponseEntity.ok(authenticationService.registerPendaftar(pendaftar));
    }

    @PostMapping(path = "/register/rekruter")
    public ResponseEntity<Object> registerRekruter(@RequestBody RekruterModel rekruter) {
        return ResponseEntity.ok(authenticationService.registerRekruter(rekruter));
    }

}
