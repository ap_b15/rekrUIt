package com.advprog2021.b15.rekruit.security.utils;

import com.advprog2021.b15.rekruit.model.UserModelRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JWTAuthResponse {
    private final String email;
    private final String token;
    private final UserModelRole role;
    private final long id;

    @Override
    public String toString() {
        return String.format(
                "{ \"email\": \"%s\", \"token\": \"%s\", \"role\": \"%s\", \"id\": \"%d\"}",
                this.getEmail(),
                this.getToken(),
                this.getRole(),
                this.getId()
        );
    }
}
