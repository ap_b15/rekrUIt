package com.advprog2021.b15.rekruit.security.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmailPasswordAuthRequest {
    private String email;
    private String password;

}
