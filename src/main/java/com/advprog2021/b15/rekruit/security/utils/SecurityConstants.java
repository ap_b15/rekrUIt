package com.advprog2021.b15.rekruit.security.utils;

public class SecurityConstants {
    public static final String SECRET = "S3M0G4_1u1us_4dpr0";
    public static final long EXPIRATION_TIME = 1800000; // 30 mins
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

}
