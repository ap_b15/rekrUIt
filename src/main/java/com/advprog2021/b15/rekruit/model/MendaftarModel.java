package com.advprog2021.b15.rekruit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Mendaftar")
public class MendaftarModel {

    @Column
    private String statusPenerimaan;

    @Column
    private String linkCV;

    @Column
    private String linkTugas;

    @Column
    private int nilai;


    @ManyToOne
    @JoinColumn(name = "judulRekruitmen")
    private RekruitmenModel rekruitmenModel;

    @ManyToOne
    @JoinColumn(name = "npm")
    private PendaftarModel pendaftarModel;

    @Id
    private String id;

    public MendaftarModel(String id, PendaftarModel pendaftarModel, RekruitmenModel rekruitmenModel, String statusPenerimaan, int nilai,String linkCV, String linkTugas){
        this.pendaftarModel = pendaftarModel;
        this.rekruitmenModel = rekruitmenModel;
        this.statusPenerimaan = statusPenerimaan;
        this.linkCV = linkCV;
        this.linkTugas = linkTugas;
        this.id = id;
        this.nilai = nilai;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
