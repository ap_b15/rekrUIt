package com.advprog2021.b15.rekruit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Rekruter")
public class RekruterModel extends UserModel implements Serializable {

    @Column
    private String namaRekruter;

    @Column
    private String deskripsiRekruter;

    @JsonIgnore
    @OneToMany(mappedBy = "rekruiter")
    private Set<RekruitmenModel> rekruitmenModelSet;
}
