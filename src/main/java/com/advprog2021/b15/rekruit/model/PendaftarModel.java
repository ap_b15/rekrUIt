package com.advprog2021.b15.rekruit.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Pendaftar")
public class PendaftarModel extends UserModel implements Serializable {

    @ManyToMany
    Set<RekruitmenModel> mendaftarRekruitmen;


    @Column(unique = true)
    private String npm;

    @Column
    private String namaLengkap;

    @Column
    private String fakultas;

    @Column
    private String kontak;

}
