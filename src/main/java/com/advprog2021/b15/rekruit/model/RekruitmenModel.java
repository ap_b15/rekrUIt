package com.advprog2021.b15.rekruit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Rekruitmen")
public class RekruitmenModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JsonIgnore
    @ManyToMany(mappedBy = "mendaftarRekruitmen", cascade = CascadeType.ALL)
    private Set<PendaftarModel> didaftarOleh;

    @ManyToOne
    @JoinColumn(name = "rekruiter", nullable = false)
    private RekruterModel rekruiter;

    @JsonIgnore
    @OneToMany(mappedBy = "rekruitmenModel")
    private Set<PengumumanModel> pengumumanModelSet;

    @Column(nullable = false)
    private String judul;

    @Column(columnDefinition = "varchar(255) default 'Belum Dibuka'")
    private String status;

    @Column(nullable = false)
    private String narahubung;

    @Column(nullable = false)
    private String deskripsiTugas;

    @Column(nullable = false)
    private String deskripsiPekerjaan;

    @Column(nullable = false)
    private String syaratKetentuan;

    @Column(nullable = false)
    private Timestamp startDateRegistrasi;

    @Column(nullable = false)
    private Timestamp dueDateRegistrasi;

    @Column(nullable = false)
    private Timestamp dueDateTugas;

    @Column(nullable = false)
    private String linkWawancara;

}
