package com.advprog2021.b15.rekruit.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name="pengumuman")
@Data
@Getter
@NoArgsConstructor
public class PengumumanModel implements Serializable {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column(name = "waktu")
    private long waktu;

    @Column(name = "judul")
    private String judul;

    @Column(name = "isi")
    private String isi;

    @ManyToOne
    @JoinColumn(name = "rekruitmenModel")
    private RekruitmenModel rekruitmenModel;

    @JsonCreator
    public PengumumanModel(@JsonProperty("judul") String judul, @JsonProperty("isi") String isi) {
        this.waktu = Timestamp.valueOf(LocalDateTime.now()).getTime();
        this.judul = judul;
        this.isi = isi;
    }
}
