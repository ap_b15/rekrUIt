# rekrUIt

[![pipeline status](https://gitlab.com/ap_b15/rekrUIt/badges/master/pipeline.svg)](https://gitlab.com/ap_b15/rekrUIt/-/commits/master)

Backend URL: `api-rekruit.herokuapp.com`
Frontend URL: `rekruit-b15.herokuapp.com`
Frontend Repository: `https://gitlab.com/ap_b15/rekruit-frontend`

Tugas Kelompok Advanced Programming 2021 B-15